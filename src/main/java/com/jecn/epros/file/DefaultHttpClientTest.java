package com.jecn.epros.file;

import com.jecn.epros.file.command.FileMessage;
import com.jecn.epros.file.command.FilePathStorage;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DefaultHttpClientTest {
    public static void main(String[] args) {

//        FileHttpClient.init(ClientMode.DEFAAULT,"http://localhost:5000");
//         FileHttpClient.init("http://148.70.11.93:3000");
        try {
            FileHttpClient.initDefaultHttpClient("http://148.70.11.93:3000");
        } catch (Exception e) {
            e.printStackTrace();
        }
        testReadDir();
        testReadFile();
        testMessage();
        testCopyDir();
    }

    private static void testCopyDir() {
        try {
            FileHttpClient.copyFileOrDir("/WX", "/唐秋月");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static void testMessage() {
        try {
            FileMessage bu = FileHttpClient.readFileMessage("bu");
            System.out.println(bu.toString());
//            for (int i = 0; i < bu.size(); i++) {
//                FilePathStorage filePathStorage = bu.get(i);
//                writeFile("D:\\a\\" + filePathStorage.getPath(), filePathStorage.getFilesName(), filePathStorage.getFileByte());
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void testReadDir() {
        try {
            List<FilePathStorage> bu = FileHttpClient.readDirOrFile("/");
//            List<FilePathStorage> sa = FileHttpClient.readDirOrFile("a - 副本.txt");
            File file = new File("D:\\project\\eps-file-util\\src\\main\\java\\com\\jecn\\epros\\file\\FileHttpClient.java");
            File[] files = file.listFiles();
            System.out.println(file.getParent());
//            for (int i = 0; i < bu.size(); i++) {
//                FilePathStorage filePathStorage = bu.get(i);
//                writeFile("D:\\a\\" + filePathStorage.getPath(), filePathStorage.getFilesName(), filePathStorage.getFileByte());
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void writeFile(String path, String fileName, byte[] content)
            throws IOException {
        try {
            File f = new File(path);
            if (!f.exists()) {
                f.createNewFile();
            }
            FileOutputStream fos = new FileOutputStream(path + fileName);
            fos.write(content);
            fos.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void testReadFile() {

        for (int i = 0; i < 100; i++) {

            FilePathStorage filePathStorage = new FilePathStorage();
            String fileName = "abc";
            String path = "/fileLibraryxz/2019111/";
            try {
                File file = new File("D:\\eprosFileLibrary\\fileLibrary\\201904\\cc6d5e71-e3e6-4f21-824c-7c542e301d8e.jpg");
                InputStream inputStream = new FileInputStream(file);
                filePathStorage.setFileByte("b8aa02b6".getBytes());
                filePathStorage.setFileByte(IOUtils.toByteArray(inputStream));
                filePathStorage.setPath(path);
                filePathStorage.setFilesName(fileName + i);
                FileHttpClient.saveFile(filePathStorage);
                byte[] bytes = FileHttpClient.readFile("filePath");
                System.out.println(Arrays.toString(bytes));
                List<String> filePaths = new ArrayList<String>();
                filePaths.add("a.txt");
                filePaths.add("b.txt");
                FileHttpClient.deleteFile(filePaths);
                byte[] da = FileHttpClient.readFile(path + fileName + i);
                System.out.println(Arrays.toString(da));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
