package com.jecn.epros.file;

import com.jecn.epros.file.client.DefaultHttpClient;
import com.jecn.epros.file.client.HttpClient;
import com.jecn.epros.file.client.shunfeng.ShunFengHttpClient;
import com.jecn.epros.file.command.FileMessage;
import com.jecn.epros.file.command.FilePathStorage;

import java.util.List;

public class FileHttpClient {
    private static HttpClient httpClient;

    public static void initDefaultHttpClient(String url){
        httpClient = new DefaultHttpClient(url);
    }

    public static void initSfClient(String ak, String userKey, String serverUrl, String containerName) throws Exception {
        httpClient = new ShunFengHttpClient(ak, userKey, serverUrl, containerName);
    }

    public static HttpClient getHttpClient() {
        if (httpClient == null) {
            throw new IllegalStateException("Need call init first");
        }
        return httpClient;
    }

    public static List<FilePathStorage> readDirOrFile(String dirOrFilePath) throws Exception {
        return getHttpClient().readDirOrFile(dirOrFilePath);
    }

    public static FileMessage readFileMessage(String dirOrFilePath) throws Exception {
        return getHttpClient().readFileMessage(dirOrFilePath);
    }

    public static byte[] readFile(String filePath) throws Exception {
        return getHttpClient().readFile(filePath);
    }

    public static void deleteFile(String filePath) throws Exception {
        getHttpClient().deleteFile(filePath);
    }

    public static void deleteFile(List<String> filePaths) throws Exception {
        getHttpClient().deleteFile(filePaths);
    }

    public static void saveFile(List<FilePathStorage> fileStorages) throws Exception {
        getHttpClient().saveFile(fileStorages);
    }

    public static void saveFile(FilePathStorage fileStorage) throws Exception {
        getHttpClient().saveFile(fileStorage);
    }

    public static void copyFileOrDir(String sourcePath, String targetPath) throws Exception {
        getHttpClient().copyFileOrDir(sourcePath, targetPath);
    }
}
