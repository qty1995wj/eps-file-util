package com.jecn.epros.file.command;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class FilePathStorage {
    @JsonIgnore
    private String filesName;

    @JsonIgnore
    private byte[] fileByte;

    private String path;

    public String getFilesName() {
        return filesName;
    }

    public void setFilesName(String filesName) {
        this.filesName = filesName;
    }

    public byte[] getFileByte() {
        return fileByte;
    }

    public void setFileByte(byte[] fileByte) {
        this.fileByte = fileByte;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
