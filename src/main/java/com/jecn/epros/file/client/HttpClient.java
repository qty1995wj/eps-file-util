package com.jecn.epros.file.client;

import com.jecn.epros.file.command.FileMessage;
import com.jecn.epros.file.command.FilePathStorage;

import java.util.List;

public interface HttpClient {

    List<FilePathStorage> readDirOrFile(String dirOrFilePath) throws Exception;

    FileMessage readFileMessage(String dirOrFilePath) throws Exception;

    byte[] readFile(String filePath) throws Exception;

    void deleteFile(String filePath) throws Exception;

    void deleteFile(List<String> filePaths) throws Exception;

    void saveFile(List<FilePathStorage> fileStorages) throws Exception;

    void saveFile(FilePathStorage fileStorage) throws Exception;

    void copyFileOrDir(String sourcePath, String targetPath) throws Exception;
}
