package com.jecn.epros.file.client.shunfeng;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.config.SocketConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import swiftsdk.SfOssClient;
import swiftsdk.SwiftConfiguration;
import swiftsdk.errors.SfOssException;
import swiftsdk.http.HttpClientFactory;
import swiftsdk.util.TokenCache;

public class EprosSfOssClient extends SfOssClient {
    private TokenCache tokenCache;

    public EprosSfOssClient(SwiftConfiguration config) {
        super(config);
        tokenCache = new TokenCache(config);
    }

    public void copy(String dir, String target) {
        String url = this.getConfiguration().getSfossServerUrl() + "/v1/AUTH_" + this.getConfiguration().getAccount()
                + "/" + this.getConfiguration().getDefaultContainer() + target;
        HttpPut put = new HttpPut(url);

        try {
            put.addHeader("X-COPY-FROM", this.getConfiguration().getDefaultContainer() + "/" + dir);
            CloseableHttpResponse response = this.doHttpBase(put, "");
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode != 202 && statusCode != 201) {
                if (statusCode == 404) {
                    throw new SfOssException("Resource does not exist");
                } else {
                }
            } else {
            }
        } catch (SfOssException var7) {
            var7.printStackTrace();
        } catch (Exception var8) {
            var8.printStackTrace();
        }
    }

    private CloseableHttpResponse doHttpBase(HttpRequestBase mothed, String style) throws Exception {
        try {
            String token = "";
            if (style.equals("v2")) {
                token = this.tokenCache.getTokenV2();
            } else {
                token = this.tokenCache.getToken();
            }

            if (token == null) {
                throw new SfOssException("无法登录Sfoss");
            } else {
                mothed.setHeader("X-Auth-Token", token);
                CloseableHttpResponse response;
                if (this.getConfiguration().getSecure()) {
                    response = HttpClientFactory.getHttpsClient().execute(mothed);
                } else {
                    int socketTimeout = 15000;
                    int connectTimeout = 15000;
                    SocketConfig socketConfig = SocketConfig.custom().setSoKeepAlive(false).setSoLinger(1).setSoReuseAddress(true).setSoTimeout(10000).setTcpNoDelay(true).build();
                    RequestConfig config = RequestConfig.custom().setConnectTimeout(connectTimeout).setSocketTimeout(socketTimeout).setConnectionRequestTimeout(connectTimeout).build();
                    CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultSocketConfig(socketConfig).setDefaultRequestConfig(config).build();
                    response = httpClient.execute(mothed);
                }

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 401) {
                    token = this.tokenCache.getToken();
                    mothed.setHeader("X-Auth-Token", token);
                    if (token == null) {
                        throw new SfOssException("无法登录Sfoss");
                    }

                    if (this.getConfiguration().getSecure()) {
                        response = HttpClientFactory.getHttpsClient().execute(mothed);
                    } else {
                        response = HttpClientFactory.getHttpClient().execute(mothed);
                    }

                    statusCode = response.getStatusLine().getStatusCode();
                    if (statusCode == 401) {
                        throw new SfOssException("无法登录Sfoss");
                    }
                }

                HttpClientFactory.getHttpClient().close();
                return response;
            }
        } catch (Exception var10) {
            throw var10;
        }
    }
}
