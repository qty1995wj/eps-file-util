package com.jecn.epros.file.client;

import com.jecn.epros.file.command.FileMessage;
import com.jecn.epros.file.command.FilePathStorage;
import com.jecn.epros.file.util.JsonUtil;
import com.jecn.epros.file.util.ZipUtil;
import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.List;

public class DefaultHttpClient implements HttpClient {

    private Logger log = Logger.getLogger(DefaultHttpClient.class);

    private static CloseableHttpClient httpClient;
    private static String FILE_SERVER_URL;

    private static CloseableHttpClient getConnection() {
        if (httpClient == null) {
            throw new IllegalStateException("Need call init first");
        }
        return httpClient;
    }

    /*
     * @param fileServerUrl if you want to use the all methods must be init()
     *                      or else throw IllegalStateException must catch
     *                      or init&&retry again
     */
    public DefaultHttpClient(String fileServerUrl) {
        RequestConfig requestConfig = RequestConfig.custom()
                .setSocketTimeout(5000)
                .setConnectTimeout(5000)
                .setConnectionRequestTimeout(5000)
                .build();
        HttpClientBuilder httpBuilder = HttpClients.custom()
                .setDefaultRequestConfig(requestConfig);
        httpClient = httpBuilder.build();
        FILE_SERVER_URL = fileServerUrl;
    }


    @Override
    public List<FilePathStorage> readDirOrFile(String dirOrFilePath) throws Exception {
        CloseableHttpClient client = getConnection();
        HttpGet httpGet = new HttpGet(FILE_SERVER_URL + "/files?path=" + URLEncoder.encode(dirOrFilePath, "utf-8"));
        CloseableHttpResponse response = null;
        InputStream in = null;
        try {
            response = client.execute(httpGet);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == HttpStatus.SC_OK) {
                HttpEntity entity = response.getEntity();
                Header[] isArchives = response.getHeaders("IS_ARCHIVE");
                String isArchive = isArchives[0].getValue();
                in = entity.getContent();
                if ("true".equals(isArchive)) {
                    return ZipUtil.unZip(in, dirOrFilePath);
                } else {
                    byte[] bytes = IOUtils.toByteArray(in);
                    File file = new File(dirOrFilePath);
                    FilePathStorage filePathStorage = new FilePathStorage();
                    filePathStorage.setFileByte(bytes);
                    filePathStorage.setPath(file.getParent());
                    filePathStorage.setFilesName(file.getName());
                    return Collections.singletonList(filePathStorage);
                }
            } else if (statusCode == HttpStatus.SC_NOT_FOUND) {
                throw new Exception("file is not exit! " + dirOrFilePath);
            }
        } catch (IOException e) {
            log.error("connect failed", e);
            throw e;
        } finally {
            if (response != null) {
                response.close();
            }
            if (in != null) {
                in.close();
            }
        }
        return null;
    }

    @Override
    public FileMessage readFileMessage(String dirOrFilePath) throws Exception {
        CloseableHttpClient client = getConnection();
        HttpGet httpGet = new HttpGet(FILE_SERVER_URL + "/files/message?path=" + URLEncoder.encode(dirOrFilePath, "utf-8"));
        CloseableHttpResponse response = null;
        try {
            response = client.execute(httpGet);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == HttpStatus.SC_OK) {
                HttpEntity entity = response.getEntity();
                StringBuilder result = new StringBuilder();
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        entity.getContent(), "UTF-8"));
                String line;
                while ((line = br.readLine()) != null) {
                    result.append(line);
                }
                br.close();
                return JsonUtil.deserialize(result.toString(), FileMessage.class);
            }
            throw new IOException("file server io exception");
        } catch (IOException e) {
            log.error("connect failed", e);
            throw e;
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }

    @Override
    public byte[] readFile(String filePath) throws Exception {
        List<FilePathStorage> filePathStorages = readDirOrFile(filePath);
        if (filePathStorages != null) {
            return filePathStorages.get(0).getFileByte();
        } else {
            return null;
        }
    }

    @Override
    public void deleteFile(String filePath) throws Exception {
        deleteFile(Collections.singletonList(filePath));
    }

    @Override
    public void deleteFile(List<String> filePaths) throws Exception {
        CloseableHttpClient client = getConnection();
        StringBuilder path = new StringBuilder();
        for (int i = 0; i < filePaths.size(); i++) {
            String filePath = filePaths.get(i);
            path.append("path=").append(URLEncoder.encode(filePath, "utf-8")).append("&");
        }
        HttpDelete httpDelete = new HttpDelete(FILE_SERVER_URL + "/files?" + path.toString());
        CloseableHttpResponse response = null;
        try {
            response = client.execute(httpDelete);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == HttpStatus.SC_OK) {
                return;
            } else if (statusCode == 400) {
                throw new Exception("file is not exit");
            }
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }


    @Override
    public void saveFile(List<FilePathStorage> fileStorages) throws Exception {
        MultipartEntityBuilder builder = MultipartEntityBuilder.create().setMode(HttpMultipartMode.RFC6532);
        for (int i = 0; i < fileStorages.size(); i++) {
            builder.addBinaryBody("files", fileStorages.get(i).getFileByte(),
                    ContentType.DEFAULT_BINARY, fileStorages.get(i).getFilesName());
        }
        String json = JsonUtil.listToJsonString(fileStorages);
        builder.addTextBody("params", json);
        HttpEntity entity = builder.build();
        HttpPost httpPost = new HttpPost(FILE_SERVER_URL + "/files/upload");
        httpPost.setEntity(entity);
        CloseableHttpClient client = getConnection();
        CloseableHttpResponse response = null;
        try {
            response = client.execute(httpPost);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == HttpStatus.SC_OK) {
                return;
            } else if (statusCode == HttpStatus.SC_NOT_FOUND) {
                throw new Exception("file save failed");
            }
        } catch (IOException e) {
            log.error("connect failed", e);
            throw e;
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }

    @Override
    public void saveFile(FilePathStorage fileStorage) throws Exception {
        List<FilePathStorage> filePathStorages = Collections.singletonList(fileStorage);
        saveFile(filePathStorages);
    }

    @Override
    public void copyFileOrDir(String sourcePath, String targetPath) throws Exception {
        CloseableHttpClient client = getConnection();
        HttpPost httpGet = new HttpPost(FILE_SERVER_URL + "/files/copy?sourcePath=" + URLEncoder.encode(sourcePath, "utf-8")
                + "&targetPath=" + URLEncoder.encode(targetPath, "utf-8"));
        CloseableHttpResponse response = null;
        try {
            response = client.execute(httpGet);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == HttpStatus.SC_NO_CONTENT) {
                return;
            } else if (statusCode == HttpStatus.SC_NOT_FOUND) {
                throw new Exception("file is not exit! " + sourcePath);
            }
        } catch (IOException e) {
            log.error("connect failed", e);
            throw e;
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }

}
