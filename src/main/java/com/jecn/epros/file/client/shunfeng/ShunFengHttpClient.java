//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.jecn.epros.file.client.shunfeng;

import com.jecn.epros.file.client.HttpClient;
import com.jecn.epros.file.command.FileMessage;
import com.jecn.epros.file.command.FilePathStorage;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import swiftsdk.SfOssClient;
import swiftsdk.SwiftConfiguration;
import swiftsdk.errors.SfOssException;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ShunFengHttpClient implements HttpClient {
    private static Logger log = Logger.getLogger(ShunFengHttpClient.class);
    private static SwiftConfiguration swiftConfiguration;
    private static SfOssClient sfOssClient;

    public ShunFengHttpClient(String ak, String userKey,String serverUrl, String containerName) throws Exception {
        try {
            swiftConfiguration = new SwiftConfiguration();
            swiftConfiguration.setAk(ak);
            swiftConfiguration.setUserKey(userKey);
            swiftConfiguration.setSfossServerUrl(serverUrl);
            sfOssClient = new SfOssClient(swiftConfiguration);
            this.createContainerIfNotExist(containerName);
            swiftConfiguration.setDefaultContainer(containerName);
        } catch (Exception var4) {
            log.error("init config failed", var4);
            throw var4;
        }
    }

    private void createContainerIfNotExist(String defaultContainerName) throws Exception {
        try {
            getConnection().headContainerMeta(defaultContainerName);
        } catch (SfOssException e) {
            if (e.getMessage().startsWith("Resource does not exist")) {
                try {
                    log.debug("create container " + defaultContainerName);
                    getConnection().createContainer(defaultContainerName);
                } catch (SfOssException e1) {
                    log.error("create container failed! " + defaultContainerName, e);
                    throw e1;
                }
            } else {
                log.error("connected failed!", e);
                throw e;
            }
        }
    }

    public SfOssClient getConnection() {
        return sfOssClient;
    }

    private FilePathStorage newFilePathStorage(String abstractPath, byte[] bytes) {
        File file = new File(abstractPath);
        FilePathStorage filePathStorage = new FilePathStorage();
        filePathStorage.setFilesName(file.getName());
        filePathStorage.setFileByte(bytes);
        filePathStorage.setPath(file.getParent());
        return filePathStorage;
    }

    @Override
    public List<FilePathStorage> readDirOrFile(String dirOrFilePath) throws Exception {
        FileMessage fileMessage = readFileMessage(dirOrFilePath);
        if (fileMessage.isExists()) {
            if (fileMessage.isDirectory()) {
                List<String> listFiles = fileMessage.getListFiles();
                List<FilePathStorage> filePathStorages = new ArrayList<FilePathStorage>();
                for (String path : listFiles) {
                    FileMessage file = readFileMessage(path);
                    if (file.isFile()) {
                        byte[] bytes = readFile(path);
                        FilePathStorage filePathStorage = newFilePathStorage(path, bytes);
                        filePathStorages.add(filePathStorage);
                    }
                }
                return filePathStorages;
            } else if (fileMessage.isFile()) {
                byte[] bytes = readFile(dirOrFilePath);
                FilePathStorage filePathStorage = newFilePathStorage(dirOrFilePath, bytes);
                return Collections.singletonList(filePathStorage);
            }
        }
        return null;
    }

    @Override
    public FileMessage readFileMessage(String dirOrFilePath) throws Exception {
        SfOssClient client = getConnection();
        try {
            log.debug("get file message!" + dirOrFilePath);
            List<String> objectList = client.getObjectList(swiftConfiguration.getDefaultContainer(),
                    "", 1000, getMoreStrictPath(dirOrFilePath), "", "/");
            log.debug("get file message! end!");
            FileMessage fileMessage = new FileMessage();
            fileMessage.setName(dirOrFilePath);
            if (objectList.size() > 0) {
                boolean isFile = false;
                fileMessage.setExists(true);
                String firstFilePath = objectList.get(0);
                if (objectList.size() == 1) {
                    if (firstFilePath.equals(dirOrFilePath)) {
                        isFile = true;
                    } else {
                        isFile = StringUtils.substringAfter(firstFilePath, dirOrFilePath).startsWith(".");
                        fileMessage.setDirectory(!isFile);
                        fileMessage.setFile(isFile);
                    }
                }
                if (!isFile) {
                    fileMessage.setListFiles(objectList);
                }
                fileMessage.setDirectory(!isFile);
                fileMessage.setFile(isFile);
                return fileMessage;
            } else {
                fileMessage.setExists(false);
                fileMessage.setDirectory(false);
                fileMessage.setExists(false);
                return fileMessage;
            }
        } catch (SfOssException e) {
            log.error("connected failed!", e);
            throw e;
        }
    }

    @Override
    public byte[] readFile(String filePath) throws Exception {
        SfOssClient client = getConnection();
        InputStream inputStream = null;
        try {
            log.debug("get file message! started" + filePath);
            inputStream = client.getObject(swiftConfiguration.getDefaultContainer(),
                    getMoreStrictPath(filePath));
            log.debug("get file message! end");
            return IOUtils.toByteArray(inputStream);
        } catch (SfOssException e) {
            if (e.getMessage().startsWith("Resource does not exist")) {
                throw new Exception("file is not exit! " + filePath);
            } else {
                log.error("connected failed!", e);
                throw e;
            }
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }

    @Override
    public void deleteFile(String filePath) throws Exception {
        SfOssClient client = getConnection();
        try {
            log.debug("delete File:" + filePath);
            client.deleteObject(swiftConfiguration.getDefaultContainer(), getMoreStrictPath(filePath));
            log.debug("delete File end!");
        } catch (SfOssException e) {
            log.error("connected failed!", e);
            throw e;
        }
    }

    @Override
    public void deleteFile(List<String> filePaths) throws Exception {
        for (String filePath : filePaths) {
            deleteFile(filePath);
        }
    }

    @Override
    public void saveFile(FilePathStorage fileStorage) throws Exception {
        SfOssClient client = getConnection();
        try {
            log.debug("save file started!"
                    + "path:" + fileStorage.getPath()
                    + "filename:" + fileStorage.getFilesName());
            client.uploadObject(swiftConfiguration.getDefaultContainer(),
                    getMoreStrictPath(fileStorage.getPath() + fileStorage.getFilesName()),
                    fileStorage.getFileByte());
            log.debug("save file ended!");
        } catch (SfOssException e) {
            if (e.getMessage().startsWith("Resource does not exist")) {
                log.error("save file failed!\n"
                        + "container name:" + swiftConfiguration.getDefaultContainer()
                        + "filename:" + fileStorage.getFilesName()
                        + "path:" + fileStorage.getPath());
            } else {
                log.error("connected failed!", e);
            }
            throw e;
        }

    }

    @Override
    public void copyFileOrDir(String sourcePath, String targetPath) throws Exception {
        List<FilePathStorage> filePathStorages = readDirOrFile(sourcePath);
        for (FilePathStorage filePathStorage : filePathStorages) {
            filePathStorage.setPath(targetPath);
        }
        saveFile(filePathStorages);
    }

    @Override
    public void saveFile(List<FilePathStorage> fileStorage) throws Exception {
        for (FilePathStorage filePathStorage : fileStorage) {
            saveFile(filePathStorage);
        }
    }

    private String getMoreStrictPath(String path) {
        if (path.startsWith("/")) {
            return StringUtils.substringAfter(path, "/");
        }
        return path;
    }
}
