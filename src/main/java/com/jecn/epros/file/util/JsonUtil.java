package com.jecn.epros.file.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.List;

public class JsonUtil {

    static Logger logger = Logger.getLogger(JsonUtil.class);

    private JsonUtil() {
        throw new IllegalStateException("Utility class");
    }

    public static <T> T deserialize(String source, Class<T> valueType) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(source, valueType);
        } catch (IOException e) {
            logger.error("deserialize {} to json failed", e);
        }
        return null;
    }

    public static String listToJsonString(List<? extends Object> source) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(source);
        } catch (JsonProcessingException e) {
            logger.error("write [" + source.toString() + "] to json failed", e);
        }
        return "write " + source.toString() + "to json failed";
    }


    public static String toJsonString(Object source) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(source);
        } catch (JsonProcessingException e) {
            logger.error("write [" + source.toString() + "] to json failed", e);
        }
        return "write " + source.toString() + "to json failed";
    }

}
