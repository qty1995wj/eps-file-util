package com.jecn.epros.file.util;

import com.jecn.epros.file.command.FilePathStorage;
import org.apache.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ZipUtil {
    static Logger logger = Logger.getLogger(JsonUtil.class);

    public static List<FilePathStorage> unZip(InputStream inputStream, String dirPath) throws IOException {
        ZipInputStream zip = new ZipInputStream(inputStream);
        ZipEntry ze;
        List<FilePathStorage> listFile = new ArrayList<FilePathStorage>();
        try {
            while ((ze = zip.getNextEntry()) != null) {
                String filename = ze.getName();
                int len;
                int size = (int) ze.getSize();
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                if (size != 0) {
                    byte[] bytes = new byte[size];
                    while ((len = zip.read(bytes, 0, size)) != -1) {
                        byteArrayOutputStream.write(bytes, 0, len);
                    }
                }
                FilePathStorage filePathStorage = new FilePathStorage();
                filePathStorage.setFilesName(filename);
                filePathStorage.setFileByte(byteArrayOutputStream.toByteArray());
                filePathStorage.setPath(dirPath);
                listFile.add(filePathStorage);
            }
            zip.close();
        } catch (IOException e) {
            logger.error("file stream read error", e);
            throw e;
        }
        return listFile;
    }
}
