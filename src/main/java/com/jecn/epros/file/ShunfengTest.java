package com.jecn.epros.file;

import com.jecn.epros.file.command.FileMessage;
import com.jecn.epros.file.command.FilePathStorage;
import com.jecn.epros.file.util.JsonUtil;

import java.util.List;

public class ShunfengTest {

    public static String fileName = "abc";
    public static String path = "/fileLibraryxz/2019111/";
    public static String notExistPath = "/tang/tang/";
    public static String notExistFile = "tang.jpg";

    static {
        try {
            String AK = "RVNHLUJQTS1DT1JFOkVTRy1CUE0tQ09SRQ==";
            String USER_KEY = "62541842c6e395f1ca0e55aa24d1380f";
            String SFOSS_SERVER_URL = "esg-bpm-core-shenzhen-xili1.osssit.sfcloud.local:8080";
            String DEFAULT_CONTAINER_NAME = "epros_files";
            FileHttpClient.initSfClient(
                    AK, USER_KEY, SFOSS_SERVER_URL, DEFAULT_CONTAINER_NAME
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) throws InterruptedException {
        Thread thread1 = new Thread(new Test());
        Thread thread2 = new Thread(new Test());
        thread1.start();
        Thread.sleep(200);
        thread2.start();
    }

    private static class Test implements Runnable {

        public void run() {
            testShunfengSaveFile();
            testShunfengReadFile();
            testShunfengReadDir();
            testGetFileMessage();
            testCopyFileOrDir();
            testDeleteFile();
        }
    }

    private static void testShunfengSaveFile() {
        try {
            FilePathStorage filePathStorage = new FilePathStorage();
            filePathStorage.setFileByte("顺丰".getBytes());
            filePathStorage.setPath(path);
            filePathStorage.setFilesName(fileName);
            FileHttpClient.saveFile(filePathStorage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void testCopyFileOrDir() {
        try {
            FileMessage fileMessage = FileHttpClient.readFileMessage(path + fileName);
            FileHttpClient.copyFileOrDir(path, "/cc/");
            List<FilePathStorage> filePathStorages = FileHttpClient.readDirOrFile("/cc/");
            System.out.println("testCopyFileOrDir:" + JsonUtil.toJsonString(filePathStorages));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private static void testGetFileMessage() {
        try {
            FileMessage fileMessage = FileHttpClient.readFileMessage(path + fileName);
            FileMessage pathMessage = FileHttpClient.readFileMessage(path);
            System.out.println("fileMessage:" + JsonUtil.toJsonString(fileMessage));
            System.out.println("pathMessage" + JsonUtil.toJsonString(pathMessage));
            FileMessage notExistFileMessage = FileHttpClient.readFileMessage(notExistPath + notExistFile);
            FileMessage notExistPathMessage = FileHttpClient.readFileMessage(notExistPath);
            System.out.println("notExistFileMessage: " + JsonUtil.toJsonString(notExistFileMessage));
            System.out.println("notExistPathMessage: " + JsonUtil.toJsonString(notExistPathMessage));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void testShunfengReadFile() {
        try {
            byte[] bytes = FileHttpClient.readFile(path + fileName);
            System.out.println(new String(bytes));
            byte[] notExistFileByte = FileHttpClient.readFile(notExistPath + notExistFile);
            System.out.println("testShunfengReadFile notExistFileByten" + new String(notExistFileByte));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void testShunfengReadDir() {
        try {
            List<FilePathStorage> filePathStorages = FileHttpClient.readDirOrFile(path);
            System.out.println("filePathStorages" + JsonUtil.toJsonString(filePathStorages.toString()));
            List<FilePathStorage> notExistFilePathStorages = FileHttpClient.readDirOrFile(notExistPath);
            System.out.println("notExistFilePathStorages" + JsonUtil.toJsonString(notExistFilePathStorages));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void testDeleteFile() {
        try {
            FileHttpClient.deleteFile(path + fileName);
            FileHttpClient.deleteFile(path);
            FileHttpClient.deleteFile(path);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
